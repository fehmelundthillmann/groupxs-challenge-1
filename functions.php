<?php

function formatPrice(float $price, $currency = "€", $currencyFirst = false)
{
    $formattedPrice = number_format($price, 2, ",", ".");
    $formattedPriceWithCurrency = $formattedPrice . " " . $currency;
    if ($currencyFirst) {
        $formattedPriceWithCurrency = $currency . " " . $formattedPrice;
    }
    return $formattedPriceWithCurrency;
}

function formIsSubmitted($variablesArray)
{
    if (isset($variablesArray["harry_books"])) {
        return true;
    }
    return false;
}

<!DOCTYPE html>
<html lang="de">
<head>
    <title>groupXS Challenge</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <h1>Wie viel kosten meine Harry Potter Bücher?</h1>
        <div class="row">
            <div class="col">
                <p>Preis pro Buch einzeln: <?php echo formatPrice(Bundle::PRICE_SINGLE) ?></p>
                <?php
                foreach (Bundle::$discountScale as $bookAmount => $discount) {
                    if ($discount > 0) {
                        echo "<p>Rabatt für " . $bookAmount . " verschiedene Bücher: " . number_format($discount, 2, ",", ".") . " %</p>";
                    }
                }
                ?>
            </div>
            <div class="col">
                <form method="post">
                    <?php for ($i = 1; $i <= 5; $i++) {
                        $value = (formIsSubmitted($variablesArray)) ? $variablesArray["harry_books"]["book" . $i] : 0;
                        ?>
                        <div class="form-group">
                            <label for="book<?=$i?>">Anzahl Band <?=$i?> "<?=$bookTitles[$i]?>"</label>
                            <input type="text" class="form-control" id="book<?=$i?>" name="harry_books[book<?=$i?>]" value="<?=$value?>">
                        </div>
                    <?php } ?>
                    <button type="submit" class="btn btn-success">
                        Berechnen
                    </button>
                </form>
                <?php if (isset($priceCalculatedNaive)) { ?>
                    <div class="spacer-vertical"></div>
                    <h4>Einfache Berechnung (nicht optimal)</h4>
                    <strong>Die Bücher sind insgesamt für <?=$priceCalculatedNaive?> zu haben. Viel Spaß beim Lesen!</strong>
                <?php } ?>
                <?php if (isset($priceCalculated)) { ?>
                    <div class="spacer-vertical"></div>
                    <h4>Optimale Berechnung</h4>
                    <strong>Die Bücher sind insgesamt für <?=$priceCalculated?> zu haben. Viel Spaß beim Lesen!</strong>
                <?php } ?>
            </div>
        </div>

    </div>
</body>

</html>
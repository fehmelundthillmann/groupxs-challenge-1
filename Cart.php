<?php

class Cart
{
    private $books = [];

    public function add($title, $number = 1)
    {
        if (!isset($this->books[$title])) {
            $this->books[$title] = 0;
        }
        $this->books[$title] += $number;
    }

    /**
     * @param bool $naive
     * @return float|int
     * @throws Exception
     */
    public function getFullPrice(bool $naive)
    {
        $naiveSolution = $this->naiveBundles();
        if ($naive)
            return $naiveSolution->minimumPrice();

        $optimalSolution = $this->optimalBundles($naiveSolution);
        return $optimalSolution->price();
    }

    /**
     * @param PotentialSolution|null $naiveSolution
     * @return mixed
     * @throws Exception
     */
    private function optimalBundles(PotentialSolution $naiveSolution = null)
    {
        /**
         * @var $potentialSolution PotentialSolution
         * @var $newBranch PotentialSolution
         */
        $potentialSolutions = Bundle::extractAllUpTo($this->books, 5)->asPotentialSolutions();
        $finished = false;
        while (!$finished) {
            $finished = true;
            $newPotentialSolutionSet = new PotentialSolutionSet();
            foreach ($potentialSolutions->getIterator() as $potentialSolution) {
                if ($naiveSolution) {
                    if ($potentialSolution->minimumPrice() > $naiveSolution->price()) {
                        continue;
                    }
                }
                if ($potentialSolution->hasRemainingBooks()) {
                    $finished = false;
                    foreach ($potentialSolution->expand(5) as $newBranch) {
                        $newPotentialSolutionSet = $newPotentialSolutionSet->add($newBranch->anonymous());
                    }
                } else {
                    $newPotentialSolutionSet = $newPotentialSolutionSet->add($potentialSolution->anonymous());
                }
            }
            $potentialSolutions = $newPotentialSolutionSet;
            $max = 0;
            foreach ($potentialSolutions as $potentialSolution) {
                $candidate = array_sum(array_values($potentialSolution->remainingBooks));
                if ($candidate > $max) {
                    $max = $candidate;
                }
            }
            file_put_contents("potentialsolutions.log","PotentialSolutionSet now contains " . count($potentialSolutions) . " potential solutions" . PHP_EOL, FILE_APPEND);
            file_put_contents("potentialsolutions.log","Maximum remaining books is " . $max . PHP_EOL, FILE_APPEND);
        }

        $solution = $potentialSolutions->bestSolution();
        return $solution;
    }

    private function naiveBundles()
    {
        $potentialSolution = new PotentialSolution([], $this->books);
        return $potentialSolution->calculateNaiveSolution();
    }
}
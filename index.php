<?php

require_once "functions.php";
require_once "Bundle.php";
require_once "BundleSet.php";
require_once "Cart.php";
require_once "PotentialSolution.php";
require_once "PotentialSolutionSet.php";

$bookTitles = array(
    1 => "Harry Potter und der Stein der Weisen",
    2 => "Harry Potter und die Kammer des Schreckens",
    3 => "Harry Potter und der Gefangene von Askaban",
    4 => "Harry Potter und der Feuerkelch",
    5 => "Harry Potter und Orden des Phönix",
);

$variablesArray = $_POST;

if (formIsSubmitted($variablesArray)) {
    $books = $variablesArray["harry_books"];
    $cart = new Cart();
    foreach ($books as $name => $amount) {
        $cart->add($name, $amount);
    }
    try {
        $priceCalculatedNaive = $cart->getFullPrice(true);
        $priceCalculatedNaive = formatPrice($priceCalculatedNaive);
        $priceCalculated = $cart->getFullPrice(false);
        $priceCalculated = formatPrice($priceCalculated);
    } catch (Throwable $e) {
        var_dump($e->getMessage());
    }
}

require_once "output.php";
